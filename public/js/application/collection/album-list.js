define(["model/album"], function(Album) {
	var AlbumList = Backbone.Collection.extend({
		url: 'http://zf2-tutorial.localhost/album-rest-api',
		model : Album
	});
	return AlbumList;
});