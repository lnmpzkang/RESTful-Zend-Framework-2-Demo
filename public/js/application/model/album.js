define([], function() {
	var Album = Backbone.Model.extend({
		idAttribute : 'id',
		urlRoot : 'http://zf2-tutorial.localhost/album-rest-api'
	});
	return Album;
});