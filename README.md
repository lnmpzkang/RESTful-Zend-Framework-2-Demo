RESTful Zend Framework 2 Demo
=======================

介绍 Introduction
------------
这是将Zend Framework 2官方Album的Demo改成一个客户端基于Require.js和Backbone.js，服务端基于Zend Framework 2的一个RESTful架构的Demo

本程序将Album demo中的CRUD功能改成了一个名为AlbumRestApiController的controller提供RESTful API，这个AlbumRestApiController继承了ZF2提供的AbstractRestfulController，它可以根据HTTP请求的method：POST，GET，PUT和DELETE来自动分发请求至CRUD的action。

在前端使用Require.js来管理js的文件，用Backbone.js来实现MVC、路由分发的功能和路由欺骗的功能，js的主要程序在/public/js/application中。

建议使用谷歌Chrome当打开应用，需要配置虚拟主机，访问http://your-domain/album-rest，在页面的header里会发现一个播放器，这个播放器是用来测试页面是否有刷新的，在播放音乐的时候对页面进行操作跳转，如果URL改变了页面也改变了但音乐没有停止播放说明程序是实现了我想要的概念——网站即应用，所有的AJAX数据请求都是访问上面提到的RESTful API，做到了URI即资源。

本程序是本人用于学习Zend Framework 2、Require.js和Backbone.js以及RESTful概念使用，欢迎各位同仁批评指正，fork点赞！谢谢！



This is a simple, RESTful application using the Require.js and Backbone.js in front-end as well as ZF2 MVC layer and module systems in back-end. This application is modified from Zend Framework 2 skeleton application.

I have modified the application from the CRUD function of Album demo to a controller named AlbumRestApiController which is provide RESTful API. The AlbumRestApiController extends AbstractRestfulController which is provided by Zend Framework 2. AbstractRestfulController could dispatch requests by HTTP method such as POST, GET, PUT and DELETE to CRUD actions.

On front-end, it uses Require.js to manage the js files and Backbone.js to implement MVC, dispatch by URL and URL trick. The js files that I write locates /public/js/application.

I suggest you to use Chrome to run this application and you need to configure a virtual host. After everything done, you need to access http://your-domain/album-rest. There is a music player in the header of the page. It is used to detect whether the page has been refreshed. When the music is playing, you could click links in the page to access every CRUD page. The URL and page are changed without music stop playing, it means that website is an application. All the AJAX requests the RESTful API that I mentioned before.

This application is used to study program with Zend Framework 2, Require.js and Backbone.js and the concept of RESTful. Thanks for your advice, Fork and Star!


